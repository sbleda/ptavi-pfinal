#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""

import socket
import sys
from xml.dom import minidom
import hashlib
import os


if __name__ == "__main__":

    try:
        OPTION = str(sys.argv[3])
        METHOD = str(sys.argv[2])
        CONFIG = str(sys.argv[1])

    except IndexError:
        sys.exit("Usage: python uaclient.py config method option ")

    doc = minidom.parse(CONFIG)
    accounts = doc.getElementsByTagName("account")
    for account in accounts:
        nameaccount = account.getAttribute("name")
        passwordaccount = account.getAttribute("password")

    uaservers = doc.getElementsByTagName("uaserver")
    for uaserver in uaservers:
        ipuaserver = uaserver.getAttribute("ip")
        portuaserver = uaserver.getAttribute("port")

    rtpaudios = doc.getElementsByTagName("rtpaudio")
    for rtpaudio in rtpaudios:
        portrtpaudio = rtpaudio.getAttribute("port")

    regproxys = doc.getElementsByTagName("regproxy")
    for regproxy in regproxys:
        ipregproxy = regproxy.getAttribute("ip")
        portregproxy = regproxy.getAttribute("port")
    audios = doc.getElementsByTagName("audio")
    for audio in audios:
        audiopath = audio.getAttribute("audiopath")

    SERVER = ipuaserver
    PORT = int(portregproxy)

    if METHOD == 'REGISTER':
        LINE1 = METHOD + ' sip:' + nameaccount + ':'
        LINE2 = LINE1 + portuaserver + ' SIP/2.0\r\n'
        LINE3 = LINE2 + 'Expires: '
        LINE = LINE3 + OPTION + ' ' + '\r\n'

    if METHOD == 'INVITE':

        li1 = '\r\n' + 'Content-Type: application/sdp'
        li2 = li1 + '\r\n' + 'v=0' + '\r\n' + 'o='
        li3 = li2 + nameaccount + ' ' + ipuaserver + '\r\n' + 's='
        li4 = li3 + nameaccount + '\r\n' + 't=0' + '\r\n' + 'm='
        li = li4 + audiopath + ' ' + portrtpaudio + ' ' + 'RTP' + '\r\n'

        LINE = METHOD + ' ' + 'sip:' + OPTION + ' ' + 'SIP/2.0\r\n' + li

    if METHOD == 'BYE':

        LINE = METHOD + ' ' + 'sip:' + OPTION + ' SIP/2.0\r\n'

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((SERVER, PORT))

        print("Enviando: " + LINE)
        my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
        data = my_socket.recv(1024)
        llega = data.decode('utf-8')
        llegaan = llega.split('\r\n')
        print(llegaan)
        try:
            if llegaan[0] == 'SIP/2.0 401 Unauthorized:':
                print('Recibido -- ', llega)
                llega0 = llega.split('=')
                print(llega0)
                llegan1 = llega0[1].split('\r\n')
                llegan4 = str(llegan1[0])
                print(llegan4)
                m = hashlib.md5()
                m.update(bytes(llegan4, 'utf-8') + b'\r\n')
                m.update(bytes(passwordaccount, 'utf-8') + b'\r\n')
                a = m.hexdigest()
                linea1 = LINE + 'Authorization: Digest response= '
                linea = linea1 + a + ' ' + '\r\n'
                print("Enviando: " + linea)
                my_socket.send(bytes(linea, 'utf-8') + b'\r\n')
                data = my_socket.recv(1024)
                llega = data.decode('utf-8')

        except IndexError:
            print(' ')

        try:
            if llegaan[0] == 'SIP/2.0 100 TRYING':
                if llegaan[2] == 'SIP/2.0 180 RING':
                    if llegaan[4] == 'SIP/2.0 200 OK':
                        ack = ('ACK' + ' sip:' + OPTION + ' SIP/2.0\r\n')
                        my_socket.send(bytes(ack, 'utf-8') + b'\r\n')
                        print(ack)
                    llegann = llegaan[12].split(' ')
                    if llegann[1] != portrtpaudio:
                        w = portrtpaudio
                        aE = './mp32rtp -i 127.0.0.1 -p '
                        aE += w + ' < ' + audiopath
                        os.system(aE)
                data = my_socket.recv(1024)
        except IndexError:
            print(' ')

    print('Recibido -- ', data.decode('utf-8'))
    print("Terminando socket...")
    print("Fin.")
