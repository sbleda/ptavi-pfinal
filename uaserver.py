#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import os
from xml.dom import minidom


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    def handle(self):
        """Clase Handle."""
        # Escribe dirección y puerto del cliente (de tupla client_address)
        # self.wfile.write(b"Hemos recibido tu peticion\r\n")
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            print("Peticion " + line.decode('utf-8'))
            # Si no hay más líneas salimos del bucle infinito
            if not line:
                break

            var = line.decode('utf-8')
            vari = var.split(" ")
            if vari[0] == 'INVITE':
                mandar = b"SIP/2.0 100 TRYING\r\n\r\n"
                mandar1 = mandar + b"SIP/2.0 180 RING\r\n\r\n"
                mandar2 = mandar1 + b"SIP/2.0 200 OK\r\n\r\n"

                li = '\r\n' + 'Content-Type: application/sdp' + '\r\n' + 'v=0'
                li2 = li + '\r\n' + 'o=' + nameaccount + ' ' + ipuaserver
                li3 = li2 + '\r\n' + 's=' + nameaccount + '\r\n' + 't=0'
                li4 = li3 + '\r\n' + 'm=' + audiopath + ' ' + portrtpaudio
                li5 = li4 + ' ' + 'RTP' + '\r\n'

                self.wfile.write(mandar2 + bytes(li5, 'utf-8'))

            elif vari[0] == 'ACK':
                w = portrtpaudio
                aE = './mp32rtp -i 127.0.0.1 -p ' + w + ' < ' + audiopath
                os.system(aE)

            elif vari[0] == 'BYE':
                self.wfile.write(bytes("SIP/2.0 200 OK\r\n\r\n", 'utf-8'))
            else:
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    try:
        CONFIG = str(sys.argv[1])

    except IndexError:
        sys.exit("Usage: python3 server.py xml")

    doc = minidom.parse(CONFIG)
    accounts = doc.getElementsByTagName("account")
    for account in accounts:
        nameaccount = account.getAttribute("name")
        passwordaccount = account.getAttribute("password")

    uaservers = doc.getElementsByTagName("uaserver")
    for uaserver in uaservers:
        ipuaserver = uaserver.getAttribute("ip")
        portuaserver = uaserver.getAttribute("port")

    rtpaudios = doc.getElementsByTagName("rtpaudio")
    for rtpaudio in rtpaudios:
        portrtpaudio = rtpaudio.getAttribute("port")

    regproxys = doc.getElementsByTagName("regproxy")
    for regproxy in regproxys:
        ipregproxy = regproxy.getAttribute("ip")
        portregproxy = regproxy.getAttribute("port")
    audios = doc.getElementsByTagName("audio")
    for audio in audios:
        audiopath = audio.getAttribute("audiopath")

    serv = socketserver.UDPServer(('', int(portuaserver)), EchoHandler)
    print("Listening...")
    serv.serve_forever()
    print("Fin.")
