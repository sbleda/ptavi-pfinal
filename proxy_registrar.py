#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""
import socketserver
import sys
import os
from xml.dom import minidom
import json
import random
import hashlib
import socket

try:
    CONFIG = (sys.argv[1])

except IndexError:
    sys.exit("Usage: python proxy_registrar.py config")


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    diccionario = {}
    diccionario1 = {}
    dicc = {}

    hashh = []

    def register2json(self):
            """Creo un json."""
            with open('database.json', 'w') as f:
                    json.dump(self.dicc, f, indent=2)

    def json2registered(self):
        """Obtengo de un json."""
        with open('database.json', 'r') as fi:
                data = json.load(fi)
                self.dicc = data

    def sercliente(self, linea, varia):
        """programa como cliente."""
        varia2 = varia[1].split(':')
        linea1 = linea.split(' ')
        linea3 = linea1[1].split(':')
        try:
            if linea1[0] == 'INVITE':
                variaa2 = varia[1].split(':')
                if self.dicc['usuario1']["name:"] == variaa2[1]:
                    port = self.dicc['usuario1']["port:"]
                    print(port)
                    pq = socket.AF_INET
                    pqq = socket.SOL_SOCKET
                    with socket.socket(pq, socket.SOCK_DGRAM) as my_socket:
                        my_socket.setsockopt(pqq, socket.SO_REUSEADDR, 1)
                        my_socket.connect(('', int(port)))

                        print("Enviando: " + linea)
                        my_socket.send(bytes(linea, 'utf-8') + b'\r\n')
                        data = my_socket.recv(1024)
                        llega = data.decode('utf-8')

                        print('Recibo:' + llega)
                    vari2 = llega.split('\r\n\r\n')
                    if vari2[0] == "SIP/2.0 100 TRYING":
                        if vari2[1] == "SIP/2.0 180 RING":
                            if vari2[2] == "SIP/2.0 200 OK":
                                lle = llega
                                self.wfile.write(bytes(lle, 'utf-8') + b'\r\n')

                if self.dicc['usuario2']["name:"] == variaa2[1]:
                    port = self.dicc['usuario2']["port:"]
                    pl = socket.AF_INET
                    pll = socket.SOL_SOCKET
                    with socket.socket(pl, socket.SOCK_DGRAM) as my_socket:
                        my_socket.setsockopt(pll, socket.SO_REUSEADDR, 1)
                        my_socket.connect(('', int(port)))

                        print("Enviando: " + linea)
                        my_socket.send(bytes(linea, 'utf-8') + b'\r\n')
                        data = my_socket.recv(1024)
                        llega = data.decode('utf-8')

                        print('Recibo:' + llega)
                        vari2 = llega.split('\r\n\r\n')
                        if vari2[0] == "SIP/2.0 100 TRYING":
                            if vari2[1] == "SIP/2.0 180 RING":
                                if vari2[2] == "SIP/2.0 200 OK":
                                    x = bytes(llega, 'utf-8') + b'\r\n'
                                    self.wfile.write(x)

            if linea1[0] == 'ACK':

                print(varia2[1])
                if self.dicc['usuario1']["name:"] == varia2[1]:
                    port = self.dicc['usuario1']["port:"]
                    pu = socket.AF_INET
                    ppu = socket.SOL_SOCKET
                    with socket.socket(pu, socket.SOCK_DGRAM) as my_socket:
                        my_socket.setsockopt(ppu, socket.SO_REUSEADDR, 1)
                        my_socket.connect(('', int(port)))

                        print("Enviando: " + linea)
                        my_socket.send(bytes(linea, 'utf-8') + b'\r\n')
                        data = my_socket.recv(1024)
                        llega = data.decode('utf-8')
                        print('Recibo:' + llega)

                    vari2 = llega.split('\r\n\r\n')
                    if vari2[0] == "SIP/2.0 200 OK":
                        self.wfile.write(bytes(llega, 'utf-8') + b'\r\n')

                if self.dicc['usuario2']["name:"] == varia2[1]:
                    port = self.dicc['usuario2']["port:"]
                    po = socket.AF_INET
                    ppo = socket.SOL_SOCKET
                    with socket.socket(po, socket.SOCK_DGRAM) as my_socket:
                        my_socket.setsockopt(ppo, socket.SO_REUSEADDR, 1)
                        my_socket.connect(('', int(port)))

                        print("Enviando: " + linea)
                        my_socket.send(bytes(linea, 'utf-8') + b'\r\n')
                        data = my_socket.recv(1024)
                        llega = data.decode('utf-8')
                        print('Recibo:' + llega)

                    vari2 = llega.split('\r\n\r\n')
                    if vari2[0] == "SIP/2.0 200 OK":
                        self.wfile.write(bytes(llega, 'utf-8') + b'\r\n')

            if linea1[0] == 'BYE':
                if self.dicc['usuario1']["name:"] == varia2[1]:
                    port = self.dicc['usuario1']["port:"]
                    pa = socket.AF_INET
                    ppa = socket.SOL_SOCKET
                    with socket.socket(pa, socket.SOCK_DGRAM) as my_socket:
                        my_socket.setsockopt(ppa, socket.SO_REUSEADDR, 1)
                        my_socket.connect(('', int(port)))

                        print("Enviando: " + linea)
                        my_socket.send(bytes(linea, 'utf-8') + b'\r\n')
                        data = my_socket.recv(1024)
                        llega = data.decode('utf-8')
                        print('Recibo:' + llega)

                    vari2 = llega.split('\r\n\r\n')
                    if vari2[0] == "SIP/2.0 200 OK":
                        self.wfile.write(bytes(llega, 'utf-8') + b'\r\n')

                if self.dicc['usuario2']["name:"] == varia2[1]:
                    port = self.dicc['usuario2']["port:"]
                    p = socket.AF_INET
                    pp = socket.SOL_SOCKET
                    with socket.socket(p, socket.SOCK_DGRAM) as my_socket:
                        my_socket.setsockopt(pp, socket.SO_REUSEADDR, 1)
                        my_socket.connect(('', int(port)))

                        print("Enviando: " + linea)
                        my_socket.send(bytes(linea, 'utf-8') + b'\r\n')
                        data = my_socket.recv(1024)
                        llega = data.decode('utf-8')
                        print('Recibo:' + llega)

                    vari2 = llega.split('\r\n\r\n')
                    if vari2[0] == "SIP/2.0 200 OK":

                        try:
                            del self.dicc['usuario1']
                        except KeyError:
                            ga = b"SIP/2.0 404 User Not Found\r\n\r\n"
                            self.wfile.write(ga)

                        self.wfile.write(bytes(llega, 'utf-8'))

        except KeyError or IndexError:
            self.wfile.write(b"SIP/2.0 404 User Not Found\r\n\r\n")

    def handle(self):
        """Clase Handle."""
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            print("Peticion " + line.decode('utf-8'))
            # Si no hay más líneas salimos del bucle infinito
            if not line:
                break

            var = line.decode('utf-8')
            vari = var.split(" ")

            doc = minidom.parse(CONFIG)
            servers = doc.getElementsByTagName("server")
            for server in servers:
                nameserver = server.getAttribute("name")
                ipserver = server.getAttribute("ip")
                portserver = server.getAttribute("port")

            if vari[0] == 'REGISTER':

                variable = vari[1]
                variable1 = variable.split(':')
                PORT = variable1[2]

                SERVER = ipserver

                doc = minidom.parse('passwords')
                ua11 = doc.getElementsByTagName("ua1")
                for ua1 in ua11:
                    nameaccount = ua1.getAttribute("nombre")
                    passacc = ua1.getAttribute("password")
                ua22 = doc.getElementsByTagName("ua2")
                for ua2 in ua22:
                    nameaccount2 = ua2.getAttribute("nombre")
                    passacc2 = ua2.getAttribute("password")

                vari1 = vari[1].split(':')

                if vari[3] == '0':
                    if nameaccount == vari1[1]:
                        try:
                            del self.dicc['usuario1']
                        except KeyError:
                            gas = b"SIP/2.0 404 User Not Found\r\n\r\n"
                            self.wfile.write(gas)
                    if nameaccount2 == vari1[1]:
                        try:
                            del self.dicc['usuario2']
                        except KeyError or IndexError:
                            gal = b"SIP/2.0 404 User Not Found\r\n\r\n"
                            self.wfile.write(gal)

                else:
                    if vari[4] == '\r\n\r\n':

                        if nameaccount == vari1[1]:
                            numrandom = random.randint(1, 10000000)
                            m = hashlib.md5()
                            m.update(bytes(str(numrandom), 'utf-8') + b'\r\n')
                            m.update(bytes(str(passacc), 'utf-8') + b'\r\n')
                            a = m.hexdigest()
                            self.hashh.append(a)
                            ja = 'SIP/2.0 401 Unauthorized:' + '\r\n\r\n'
                            ja2 = ja + 'WWW Authenticate: Digest nonce='
                            ja3 = ja2 + str(numrandom)
                            self.wfile.write(bytes(ja3, 'utf-8') + b'\r\n')

                        if nameaccount2 == vari1[1]:
                            numrandom = random.randint(1, 10000000)
                            m = hashlib.md5()
                            m.update(bytes(str(numrandom), 'utf-8') + b'\r\n')
                            m.update(bytes(str(passacc2), 'utf-8') + b'\r\n')
                            a = m.hexdigest()
                            self.hashh.append(a)
                            ja = 'SIP/2.0 401 Unauthorized:\r\n\r\n'
                            ja2 = ja + 'WWW Authenticate: Digest nonce='
                            ja3 = ja2 + str(numrandom)
                            self.wfile.write(bytes(ja3, 'utf-8') + b'\r\n')

                    if vari[4] == '\r\nAuthorization:':
                        if nameaccount == vari1[1]:
                            for has in self.hashh:
                                if vari[7] == has:
                                    self.diccionario['name:'] = variable1[1]
                                    self.diccionario['ip:'] = '127.0.0.1'
                                    self.diccionario['port:'] = vari1[2]
                                    self.diccionario['password:'] = passacc
                                    self.dicc['usuario1'] = self.diccionario
                                    print(self.dicc)
                            a = 'SIP/2.0 200 OK\r\n'
                            self.wfile.write(bytes(a, 'utf-8') + b'\r\n')

                        if nameaccount2 == vari1[1]:
                            for has in self.hashh:
                                if vari[7] == has:
                                    self.diccionario1['name:'] = variable1[1]
                                    self.diccionario1['ip:'] = '127.0.0.1'
                                    self.diccionario1['port:'] = vari1[2]
                                    self.diccionario1['password:'] = passacc2
                                    self.dicc['usuario2'] = self.diccionario1
                            a = 'SIP/2.0 200 OK'
                            self.wfile.write(bytes(a, 'utf-8') + b'\r\n')

            if vari[0] == 'INVITE':

                EchoHandler.sercliente(self, str(var), vari)

            if vari[0] == 'ACK':

                EchoHandler.sercliente(self, str(var), vari)

            if vari[0] == 'BYE':

                EchoHandler.sercliente(self, str(var), vari)

        EchoHandler.register2json(self)
        EchoHandler.json2registered(self)


if __name__ == "__main__":

    # Creamos servidor de eco y escuchamos

    serv = socketserver.UDPServer(('', 6076), EchoHandler)
    print("Listening...")
    serv.serve_forever()
    print("Fin.")
